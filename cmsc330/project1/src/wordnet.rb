require_relative "graph.rb"

class Synsets

    def initialize
        @set = Hash.new
    end

    def load(synsets_file)
        fp = open(synsets_file)
        count = 0
        invalid = []
        while ((line = fp.gets) && count+=1)
            line = line.chomp
            check = false
            if (!line.match(/^id: ([0-9]+) synset: ([A-Za-z0-9_\-.'\/,]+)$/) || line.match(/^id: ([0-9]+) synset: (,+)$/))
                invalid.append(count)
                check = true
            end
        end
        if check
            return invalid
        end
        fp = open(synsets_file)
        count = 0
        while ((line = fp.gets))
            line = line.chomp
            if line.match (/^id: ([0-9]+) synset: (,+)$/)
            elsif (line.match(/^id: ([0-9]+) synset: ([A-Za-z0-9_\-.'\/,]+)$/))
                id = $1
                array = $2.split(",")
                addSet(id.to_i, array)
            else
            end
        end
        return invalid unless invalid.length == 0
        return nil
    end

    def addSet(synset_id, nouns)
        if (synset_id < 0 || nouns.length == 0 || @set[synset_id])
            return false
        end
        @set[synset_id] = nouns   
        return true
    end

    def lookup(synset_id)
        if (!@set[synset_id])
            return []
        else
            return @set[synset_id]
        end
    end

    def findSynsets(to_find)
        if to_find.class == String
            to_return = []
            @set.each do |x|
                if (x[1].count(to_find) > 0)
                    to_return.append(x[0])
                end
            end
            return to_return
        elsif to_find.class == Array
            to_return = Hash.new
            to_find.each do |x|
                to_return[x] = findSynsets(x)
            end
        end
        return to_return
    end
end

class Hypernyms
    def initialize
        @set = Hash.new
        @graph = Graph.new
    end

    def load(hypernyms_file)
        fp = open(hypernyms_file)
        count = 0
        invalid = []
        while ((line = fp.gets) && count+=1)
            line = line.chomp
            if(line.match(/^from: ([0-9]+) to: (,+)$/))
                invalid.append(count)
            elsif (line.match(/^from: ([0-9]+) to: ([0-9,]+)*$/))
                id = $1
                array = $2.split(",")
                array.each do |x|
                    addHypernym(id.to_i, x.to_i)
                end
            else
                invalid.append(count)
            end
        end
        return invalid unless invalid.length == 0
        return nil
    end

    def addHypernym(source, destination)
        if(source == destination || source < 0 || destination < 0)
            return false
        end
        if (!@graph.hasVertex?(source))
            @graph.addVertex(source)
        end
        if (!@graph.hasVertex?(destination))
            @graph.addVertex(destination)
        end
        if (!@graph.hasEdge?(source,destination))
            @graph.addEdge(source,destination)
        end
        return true
    end

    def lca(id1, id2)
        if (!(@graph.hasVertex?(id1)) || !(@graph.hasVertex?(id2)))
            return nil
        end
        hash_one = @graph.bfs(id1)
        hash_two = @graph.bfs(id2)
        to_return = []
        hash_one.keys.each do |x|
            if hash_two[x] != nil && to_return.length == 0
                to_return.append x
            elsif hash_two[x] != nil && to_return.length > 0
                if (hash_one[to_return[0]] + hash_two[to_return[0]] > hash_one[x] + hash_two[x])
                    to_return.clear
                    to_return.append x
                elsif (hash_one[to_return[0]] + hash_two[to_return[0]] == hash_one[x] + hash_two[x])
                    to_return.append x
                end
            end
        end
        return to_return
    end
end

class CommandParser
    def initialize
        @synsets = Synsets.new
        @hypernyms = Hypernyms.new
    end

    def parse(command)
        hash = Hash.new
        if (command.match(/^\s*load/))
            hash[:recognized_command] = :load
            if command.match(/^\s*load\s+([a-zA-Z_\/\-.0-9]+)\s+([a-zA-Z_\/\-.0-9]+)\s*$/)
                param_one = $1
                param_two = $2
                fp = open(param_one)
                while (line = fp.gets)
                    line = line.chomp
                    if (!line.match(/^id: ([0-9]+) synset: ([A-Za-z0-9_\-.'\/,]+)$/) || line.match(/^id: ([0-9]+) synset: (,+)$/))
                        hash[:result] = false
                        return hash
                    end
                end
                fp = open(param_two)
                while (line = fp.gets)
                    line = line.chomp
                    if (!line.match(/^from: ([0-9]+) to: ([0-9,]+)*$/) || line.match(/^from: ([0-9]+) to: (,+)$/))
                        hash[:result] = false
                        return hash
                    end
                end
                @synsets.load param_one
                @hypernyms.load param_two
                hash[:result] = true
            else
                hash[:result] = :error
            end
            return hash
        elsif (command.match(/^\s*lca/))
            hash[:recognized_command] = :lca
            if command.match(/^\s*lca\s+([0-9]+)\s+([0-9]+)\s*$/)
                hash[:result] = @hypernyms.lca($1.to_i, $2.to_i)
            else
                hash[:result] = :error
            end
            return hash
        elsif (command.match(/^\s*find\s/))
            if command.match(/^\s*find\s+([A-Za-z0-9_\-.'\/]+)\s*$/)
                hash[:result] = @synsets.findSynsets($1)
            else
                hash[:result] = :error
            end
            hash[:recognized_command] = :find
            return hash
        elsif (command.match(/^\s*findmany/))
            if(command.match(/^\s*findmany\s+(,+)\s*$/))
                hash[:result] = :error
            elsif (command.match(/^\s*findmany\s+([A-Za-z0-9_\-.'\/,]+)\s*$/))
                hash[:result] = @synsets.findSynsets($1.split(","))
            else
                hash[:result] = :error
            end
            hash[:recognized_command] = :findmany
            return hash
        elsif (command.match(/^\s*lookup/))
            if command.match(/^\s*lookup\s+([0-9]+)\s*$/)
                hash[:result] = @synsets.lookup($1.to_i)
            else
                hash[:result] = :error
            end
            hash[:recognized_command] = :lookup
            return hash
        else
            return {:recognized_command => :invalid }
        end
    end
end
